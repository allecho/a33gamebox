#ifndef CDX_VERSION_H
#define CDX_VERSION_H

#ifdef __cplusplus
extern "C" {
#endif

static const char* CDX_PLATFORM = "A33-KK44";
static const char* CDX_SVN_REPOSITORY = "svn://172.16.1.11/VideoLib/branches/A33/PAD/KK44/CedarX-Projects";
static const char* CDX_SVN_VERSION = "6827";
static const char* CDX_SVN_COMMITTER = "yaosen";
static const char* CDX_SVN_DATE = "2014-08-06 16:27:21 +0800 (三, 06  8月 2014)";
static const char* CDX_RELEASE_AUTHOR = "sujiachang";

#ifdef __cplusplus
}
#endif

#endif

