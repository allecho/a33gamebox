#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<termios.h>
#include<errno.h>

#include "encryptboot.h"

#define	FALSE	-1
#define TRUE	0

int speed_arr[] = { B115200, B57600, B38400, B19200, B9600, B4800, B2400, B1200, B300,
                    B38400, B19200, B9600, B4800, B2400, B1200, B300,
                  };
int name_arr[] = {115200, 57600, 38400,  19200,  9600,  4800,  2400,  1200,  300,
                  38400,  19200,  9600, 4800, 2400, 1200,  300,
                 };

void set_speed(int fd, int speed)
{
    int   i;
    int   status;
    struct termios   Opt;
    tcgetattr(fd, &Opt);
    for ( i = 0;  i < sizeof(speed_arr) / sizeof(int);  i++) {
        if  (speed == name_arr[i]) {
            tcflush(fd, TCIOFLUSH);
            cfsetispeed(&Opt, speed_arr[i]);
            cfsetospeed(&Opt, speed_arr[i]);
            status = tcsetattr(fd, TCSANOW, &Opt);
            if  (status != 0)
                printf("tcsetattr fd\n");
            return;
        }
        tcflush(fd, TCIOFLUSH);
    }
}

int set_Parity(int fd, int databits, int stopbits, int parity)
{
    struct termios options;
    if  ( tcgetattr( fd, &options)  !=  0) {
        printf("SetupSerial\n");
        return(FALSE);
    }
    options.c_cflag &= ~CSIZE;
    switch (databits) {
    case 7:
        options.c_cflag |= CS7;
        break;
    case 8:
        options.c_cflag |= CS8;
        break;
    default:
        printf("Unsupported data size\n");
        return (FALSE);
    }
    switch (parity) {
    case 'n':
    case 'N':
        options.c_cflag &= ~PARENB;
        options.c_iflag &= ~INPCK;
        break;
    case 'o':
    case 'O':
        options.c_cflag |= (PARODD | PARENB);
        options.c_iflag |= INPCK;
        break;
    case 'e':
    case 'E':
        options.c_cflag |= PARENB;
        options.c_cflag &= ~PARODD;
        options.c_iflag |= INPCK;
        break;
    case 'S':
    case 's':
        options.c_cflag &= ~PARENB;
        options.c_cflag &= ~CSTOPB;
        break;
    default:
        printf("Unsupported parity\n");
        return (FALSE);
    }

    switch (stopbits) {
    case 1:
        options.c_cflag &= ~CSTOPB;
        break;
    case 2:
        options.c_cflag |= CSTOPB;
        break;
    default:
        printf("Unsupported stop bits\n");
        return (FALSE);
    }

	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
	options.c_oflag &= ~OPOST;
	options.c_iflag &= ~(IXON | IXOFF | IXANY);
	options.c_iflag &= ~(INLCR | ICRNL /* | IGNCR*/);
	
	options.c_oflag &= ~(ONLCR | OCRNL);


    if (parity != 'n')
        options.c_iflag |= INPCK;
    options.c_cc[VTIME] = 50;
    options.c_cc[VMIN] = 0;

    tcflush(fd, TCIFLUSH);
    if (tcsetattr(fd, TCSANOW, &options) != 0) {
        printf("SetupSerial\n");
        return (FALSE);
    }
    return (TRUE);
}

int encryptboot(void)
{
    
	unsigned char read_test[512] = {
        0xF3, 0x0D, 0x0C, 0xF4,
        0xF1, 0x0F, 0x0E, 0xF2,
        0x01, 0x02, 0x03, 0x04,
        0x01, 0x00,
        0x0C,
        0xF4, 0x0F, 0x07, 0xF2, 0x11, 0xF3, 0x0D, 0x09, 0xF4, 0x67, 0x89, 0x00,
        0x10, 0xfb, 0xff, 0xff,
        0xF3, 0x0D, 0x0C, 0xF4
    };
	
	
	int fd;

    int i, n, nread, j = 0;
	unsigned int crc;
    int frame_flag = 0;
    unsigned char read_tmp[30];
    unsigned char read_data[512] = {0};
    char *dev = "/dev/ttyS2";
    fd = open(dev, O_RDWR | O_NOCTTY );//O_NDELAY);
    if (fd > 0)
        set_speed(fd, 115200);
    else {
        printf("Can't Open Serial Port!\n");
        return FALSE;
    }
    if (set_Parity(fd, 8, 1, 'N') == FALSE) {
        printf("Set Parity Error\n");
        return FALSE;
    }
	write(fd, read_test,sizeof(read_test));
    memset(read_tmp, 0, sizeof(read_tmp));
    if (nread = read(fd, read_tmp, sizeof(read_tmp)) > 0 ) {
		for (i = 0; i <= 30; i++) {
			printf("read_buff[%d] = 0x%02x\n", i, read_tmp[i]);
		}
		
        for (i = 0; i < sizeof(read_tmp); i++) {
            if (read_tmp[i] == 0xFA && read_tmp[i + 1] == 0x06 && read_tmp[i + 2] == 0x0D && read_tmp[i + 3] == 0xF3) {
                if (frame_flag == 1) {
                    read_data[j] = read_tmp[i];
                    j++;
                } else {
                    j = 0;
                    memset(read_data, 0, sizeof(read_data));
                    for (n = 0; n < 4; n++) {
                        read_data[j + n] = read_tmp[i + n];
                    }
                    j = j + n;
                    i = i + n - 1;
                    frame_flag = 1;
                }
            } else if (read_tmp[i] == 0xF3 && read_tmp[i + 1] == 0x0D && read_tmp[i + 2] == 0x06/* && read_tmp[i + 3] == 0xFA*/) {
                for (n = 0; n < 4; n++) {
                    read_data[j + n] = read_tmp[i + n];
                }
                if (frame_flag == 1) {
                    break;
                }
            } else {
                read_data[j] = read_tmp[i];
                j++;
            }
        }

        for (i = 0; i < 31; i++) {
            switch(i) {
            case 0:
                if (read_data[i] == 0xFA && read_data[i + 1] == 0x06 && read_data[i + 2] == 0x0D && read_data[i + 3] == 0xF3) {
                    printf("FirstFrame is right\n");
                    i = i + 21;
                } else {
                    goto shutdown_device;
                }
			case 23:
                crc = (read_data[4] + read_data[5] + read_data[6] + read_data[7]) - (read_data[8] + read_data[9]) - (read_data[11] + read_data[12] + read_data[13] + read_data[14] + read_data[15] + read_data[16] + read_data[17] + read_data[18] + read_data[19] + read_data[20] + read_data[21] + read_data[22]);
                //printf("crc0 = 0x%02x   crc1 = 0x%02x  crc2 = 0x%02x  crc3 = 0x%02x\n", crc & 0xff, (crc >> 8) & 0xff, (crc >> 16) & 0xff, (crc >> 24) & 0xff);
                if (((crc & 0xFF) == read_data[23]) && (((crc >> 8) & 0xFF) == read_data[24]) && (((crc >> 16) & 0xFF) == read_data[25]) && (((crc >> 24) & 0xFF) == read_data[26])) {
                    printf("CRC Decrytion success\n");
                    i = i + 6;
                } else {
					goto shutdown_device;
                }
            case 27:
                if (read_data[i] == 0xF3 && read_data[i + 1] == 0x0D && read_data[i + 2] == 0x06 /*&& read_data[i + 3] == 0xFA*/) {
                    printf("LastFrame is right\n");
                } else {
                    goto shutdown_device;
                }
            }
        }

    } else {
		goto shutdown_device;
	}
    close(fd);
    printf("Decrytion success\n");
    return TRUE;
shutdown_device:
    printf("Decrytion failure,Immediately shutdown\n");
    return -1;
}

