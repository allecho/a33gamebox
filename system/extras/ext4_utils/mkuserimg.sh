#!/bin/bash -x
#
# To call this script, make sure make_ext4fs is somewhere in PATH

function usage() {
cat<<EOT
Usage:
mkuserimg.sh [-s] SRC_DIR OUTPUT_FILE EXT_VARIANT MOUNT_POINT SIZE [FILE_CONTEXTS]
EOT
}

function delete_app() {
set +x

delete_app_array=(
#BasicDreams.apk
Bluetooth.apk
Browser.apk
Calculator.apk
Calendar.apk
Camera2.apk
#CertInstaller.apk
DeskClock.apk
Development.apk
DocumentsUI.apk
DownloadProviderUi.apk
Email.apk
#Exchange2.apk
Galaxy4.apk
Gallery2.apk
HoloSpiralWallpaper.apk
#HomeLauncher.apk
HTMLViewer.apk
#KeyChain.apk
LatinIME.apk
LiveWallpapers.apk
LiveWallpapersPicker.apk
MagicSmokeWallpapers.apk
Music.apk
NoiseField.apk
OpenWnn.apk
#PackageInstaller.apk
#PacProcessor.apk
#PartnerBookmarksProvider.apk
#PhaseBeam.apk
PhotoTable.apk
PinyinIME.apk
PrintSpooler.apk
#Provision.apk
QuickSearchBox.apk
SoundRecorder.apk
SpeechRecorder.apk
TelephonyProvider.apk
#upgrade66.apk
#Uplogo.apk
#UserDictionaryProvider.apk
VideoEditor.apk
VisualizationWallpapers.apk
WAPPushManager.apk
#BackupRestoreConfirmation.apk
CalendarProvider.apk
Contacts.apk
ContactsProvider.apk
#DefaultContainerService.apk
DownloadProvider.apk
#ExternalStorageProvider.apk
#FusedLocation.apk
#InputDevices.apk
Keyguard.apk
Launcher2.apk
#MediaProvider.apk
MusicFX.apk
#OneTimeInitializer.apk
ProxyHandler.apk
#Settings.apk
#SettingsProvider.apk
#SharedStorageBackup.apk
Shell.apk
#SystemUI.apk
TeleService.apk
VpnDialogs.apk
WallpaperCropper.apk
)

for (( i=0;i<${#delete_app_array[@]};i=i+1))
	do
		if [ -f ${SRC_DIR}/app/${delete_app_array[i]} ]; then
			rm ${SRC_DIR}/app/${delete_app_array[i]}
		fi
		if [ -f ${SRC_DIR}/priv-app/${delete_app_array[i]} ]; then
			rm ${SRC_DIR}/priv-app/${delete_app_array[i]}
		fi
done

set -x
}

echo "in mkuserimg.sh PATH=$PATH"

ENABLE_SPARSE_IMAGE=
if [ "$1" = "-s" ]; then
  ENABLE_SPARSE_IMAGE="-s"
  shift
fi

if [ $# -ne 5 -a $# -ne 6 ]; then
  usage
  exit 1
fi

SRC_DIR=$1
if [ ! -d $SRC_DIR ]; then
  echo "Can not find directory $SRC_DIR!"
  exit 2
fi

OUTPUT_FILE=$2
EXT_VARIANT=$3
MOUNT_POINT=$4
SIZE=$5
FC=$6

case $EXT_VARIANT in
  ext4) ;;
  *) echo "Only ext4 is supported!"; exit 3 ;;
esac

if [ -z $MOUNT_POINT ]; then
  echo "Mount point is required"
  exit 2
fi

if [ -z $SIZE ]; then
  echo "Need size of filesystem"
  exit 2
fi

if [ -n "$FC" ]; then
    FCOPT="-S $FC"
fi

delete_app

MAKE_EXT4FS_CMD="make_ext4fs $ENABLE_SPARSE_IMAGE $FCOPT -l $SIZE -a $MOUNT_POINT $OUTPUT_FILE $SRC_DIR"
echo $MAKE_EXT4FS_CMD
$MAKE_EXT4FS_CMD
if [ $? -ne 0 ]; then
  exit 4
fi
