_kernel_intermediates := vendor/softwinner/linux-3.4

BUILT_KERNEL_TARGET := $(_kernel_intermediates)/arch/arm/boot/zImage

# At this stage, BUILT_SYSTEMIMAGE in $TOP/build/core/Makefile has not
# yet been defined, so we cannot rely on it.
_systemimage_intermediates_kmodules := \
    $(call intermediates-dir-for,PACKAGING,systemimage)
BUILT_SYSTEMIMAGE_KMODULES := $(_systemimage_intermediates_kmodules)/system.img

INSTALLED_SYSTEMIMAGE_KMODULES := $(PRODUCT_OUT)/system.img

# Following dependency is already defined in $TOP/build/core/Makefile,
# but for the sake of clarity let's re-state it here. This dependency
# causes following dependencies to be indirectly defined:
#   $(NV_INSTALLED_SYSTEMIMAGE): kmodules $(BUILT_KERNEL_TARGET)
# which will prevent too early creation of systemimage.
$(INSTALLED_SYSTEMIMAGE_KMODULES): $(BUILT_SYSTEMIMAGE_KMODULES)

.PHONY: kmodules $(BUILT_KERNEL_TARGET)

$(INSTALLED_KERNEL_TARGET): $(BUILT_KERNEL_TARGET) | $(ACP)
	$(copy-file-to-target)

# Unless we hardcode the list of kernel modules, we cannot create
# a proper dependency from systemimage to the kernel modules.
# If we decide to hardcode later on, BUILD_PREBUILT (or maybe
# PRODUCT_COPY_FILES) can be used for including the modules in the image.
# For now, let's rely on an explicit dependency.
$(BUILT_SYSTEMIMAGE_KMODULES):kmodules

$(BUILT_KERNEL_TARGET): FORCE
	cd $(TOP)/vendor/softwinner/ && cp -f build/buildconfig .buildconfig && ./build.sh
	cp -rf $(_kernel_intermediates)/output/lib/modules/*/nand.ko  $(TARGET_OUT)/../root/nand.ko
	cp -rf $(_kernel_intermediates)/output/lib/modules/*/disp.ko  $(TARGET_OUT)/../root/disp.ko
	cp -rf $(_kernel_intermediates)/output/lib/modules/*/lcd.ko  $(TARGET_OUT)/../root/lcd.ko
	cp -rf $(_kernel_intermediates)/output/lib/modules/*/lcd.ko  $(TARGET_OUT)/../obj/lcd.ko
	cp -rf $(_kernel_intermediates)/output/lib/modules/*/disp.ko  $(TARGET_OUT)/../obj/disp.ko
	cp -rf $(_kernel_intermediates)/output/lib/modules/*/nand.ko  $(TARGET_OUT)/../obj/nand.ko
#	cp -rf $(_kernel_intermediates)/output/lib/modules/*/sw-device.ko  $(TARGET_OUT)/../obj/sw-device.ko
#	cp -rf $(_kernel_intermediates)/output/lib/modules/*/sunxi-keyboard.ko  $(TARGET_OUT)/../obj/sunxi-keyboard.ko


######################add cp some touch ko to recovery mode
#	cp -rf $(_kernel_intermediates)/../out/android/common/lib/modules/*/sw_device.ko  $(TARGET_OUT)/../sw_device.ko
#	cp -rf $(_kernel_intermediates)/drivers/input/sw_touchscreen/*.ko  $(TARGET_OUT)/../		
#	cp -rf $(_kernel_intermediates)/../out/android/common/bImage $(TARGET_OUT)/kernel
	

# This will add all kernel modules we build for inclusion the system
# image - no blessing takes place.
kmodules: $(INSTALLED_KERNEL_TARGET) FORCE
	@echo "Kernel modules install"
	if [ ! -d $(TARGET_OUT)/vendor/modules ]; then mkdir -p  $(TARGET_OUT)/vendor/modules; fi
	cp -rf $(_kernel_intermediates)/output/lib/modules/*/*  $(TARGET_OUT)/vendor/modules

