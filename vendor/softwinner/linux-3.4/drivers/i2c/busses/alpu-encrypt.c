/*
 * alpu_encrypt.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/mutex.h>
#include <linux/sysfs.h>
#include <linux/mod_devicetable.h>
#include <linux/log2.h>
#include <linux/bitops.h>
#include <linux/jiffies.h>
#include <linux/of.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/random.h>
#include <linux/reboot.h>

extern unsigned char alpuc_process(unsigned char *, unsigned char *);
unsigned char _alpu_rand(void);
void _alpu_delay_ms(unsigned int i);
unsigned char _i2c_read(unsigned char device_addr, unsigned char sub_addr, unsigned char *buff, int ByteNo);
unsigned char _i2c_write(unsigned char device_addr, unsigned char sub_addr, unsigned char *buff, int ByteNo);

#define ALPU_ATTR(_name)       \
{									\
	.attr = { .name = #_name,.mode = 0444 },    \
	.show =  _name##_show,          \
}

struct i2c_client *alpu_client;

static const struct i2c_device_id alpu_encrypt_ids[] = {
	{ "alpu_encrypt", 0 },
	{ /* END OF LIST */ }
};
MODULE_DEVICE_TABLE(i2c, alpu_encrypt_ids);

void _alpu_delay_ms(unsigned int i)
{
	mdelay(i);
}

unsigned char _alpu_rand(void)
{
	u8 buf[2];
	int i = 0;

	for(i=0; i<2; i++) {
		get_random_bytes(&buf[i], sizeof(u8));
	}
	return buf[0];
}

static int alpu_encrypt_i2c_rxdata(char *rxdata, int length)
{
	int ret;

	struct i2c_msg msgs[] = {
		{
			.addr	= alpu_client->addr,
			.flags	= 0,
			.len	= 1,
			.buf	= rxdata,
		},
		{
			.addr	= alpu_client->addr,
			.flags	= I2C_M_RD,
			.len	= length,
			.buf	= rxdata,
		},
	};

	ret = i2c_transfer(alpu_client->adapter, msgs, 2);
	if (ret < 0) 
		pr_info("%s i2c read alpu_encrypt error: %d\n", __func__, ret);

	return ret;
}

static int alpu_encrypt_i2c_txdata(char *txdata, int length)
{
	int ret;

	struct i2c_msg msg[] = {
		{
			.addr	= alpu_client->addr,
			.flags	= 0,
			.len	= length,
			.buf	= txdata,
		},
	};

	ret = i2c_transfer(alpu_client->adapter, msg, 1);
	if (ret < 0)
		pr_err("%s i2c write alpu_encrypt error: %d\n", __func__, ret);

	return 0;
}

static ssize_t read_show(struct kobject *kobj, struct kobj_attribute *attr,
			char *buf)
{
  	u8 rxdata[4];

	rxdata[0] = 0x1;
  	alpu_encrypt_i2c_rxdata(rxdata, 3);

	return sprintf(buf, "Read data : 0x%x, 0x%x, 0x%x, 0x%x\n",
				rxdata[0], rxdata[1], rxdata[2], rxdata[3]);
}

static ssize_t write_show(struct kobject *kobj, struct kobj_attribute *attr,
			char *buf)
{
	static u8 txdata[4] = {0x1, 0xAA, 0xBB, 0xCC};

	txdata[1]++;
	txdata[2]++;
	txdata[3]++;
	alpu_encrypt_i2c_txdata(txdata,4);

	return sprintf(buf, "Write data: 0x%x, 0x%x, 0x%x, 0x%x\n",
				txdata[0], txdata[1], txdata[2], txdata[3]);
}

static struct kobj_attribute read	= ALPU_ATTR(read);
static struct kobj_attribute write	= ALPU_ATTR(write);

static const struct attribute *alpu_attrs[] = {
	&read.attr,
	&write.attr,
	NULL,
};
unsigned char _i2c_read(unsigned char device_addr, unsigned char sub_addr, unsigned char *buf, int ByteNo)
{
	int i = 0;
	unsigned char rxdata[24] = {0};
	rxdata[0] = sub_addr;
	alpu_encrypt_i2c_rxdata(rxdata, ByteNo);
	
	for(i=0; i<ByteNo; i++) {
		buf[i] = rxdata[i];
	}
	return 0;
}
unsigned char _i2c_write(unsigned char device_addr, unsigned char sub_addr, unsigned char *buf, int ByteNo)
{
	int i = 0;
	unsigned char w_buf[24] = {0};
	for (i=0; i<ByteNo; i++) {
		w_buf[i+1] = buf[i];
	}
	w_buf[0] = sub_addr;
	alpu_encrypt_i2c_txdata(w_buf, ByteNo + 1);
	return 0;
}

static int alpu_encrypt_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	int err;
	int i;
	alpu_client = client;
	err = sysfs_create_files(&client->dev.kobj,alpu_attrs);
	if(err){
		printk("sysfs_create_files failed\n");
	}

	unsigned char error_code;
	unsigned char dx_data[8];
	unsigned char tx_data[8];
	for(i=0; i<8; i++) {	
		tx_data[i] = _alpu_rand();
		dx_data[i] = 0;
	}
	error_code = alpuc_process(tx_data,dx_data);
	if (error_code == 0) {
		printk("ALPU Encrypt OK!\n");
	} else {
		printk("ALPU Encrypt Error!\n");
		kernel_power_off();
	}
	return 0;
}

static int alpu_encrypt_remove(struct i2c_client *client)
{
	sysfs_remove_files(&client->dev.kobj,alpu_attrs);
	return 0;
}

/*-------------------------------------------------------------------------*/

static struct i2c_driver alpu_encrypt_driver = {
	.driver = {
		.name = "alpu_encrypt",
		.owner = THIS_MODULE,
	},
	.probe = alpu_encrypt_probe,
	.remove = alpu_encrypt_remove,
	.id_table = alpu_encrypt_ids,
};

static int __init alpu_encrypt_init(void)
{
	printk("+++ JonLee %s()	%d\n", __func__, __LINE__);
	return i2c_add_driver(&alpu_encrypt_driver);
}


static void __exit alpu_encrypt_exit(void)
{
	printk("+++ JonLee %s()	%d\n", __func__, __LINE__);
	i2c_del_driver(&alpu_encrypt_driver);
}

module_init(alpu_encrypt_init);
module_exit(alpu_encrypt_exit);

MODULE_DESCRIPTION("Neowine ALPU Encrypt Driver");
MODULE_AUTHOR("jonleea@163.com");
MODULE_LICENSE("GPL");
